<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * First controller for routing
 */
class LeapYearController
{
    /**
     * @param Request $request
     * @return Response
     */
    public function index(Request $request):Response
    {
        return render_template($request);
    }
}