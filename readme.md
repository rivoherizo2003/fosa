Create framework base on this tutorial https://symfony.com/doc/current/create_framework

Install dependencies
===================
```bash
composer install
symfony server:start --port=4321
```

Launch
===================
http://localhost:4321/hello/{name}
